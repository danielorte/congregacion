<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::post('login', 'UserController@login');
//Route::post('register', 'UserController@register');
Route::get('blocks/notify', 'BlockController@notify');
Route::get('service_reports/notify', 'ServiceReportController@notify');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function(){
    Route::put('/users/{user}/settings','UserController@settings');
    Route::get('/permissions', 'PermissionController@index');
    Route::resource('/blocks', 'BlockController');
    Route::resource('/block_reports', 'BlockReportController');
    Route::resource('/territories', 'TerritoryController');
    Route::resource('/preachers', 'PreacherController');
    Route::resource('/groups', 'GroupController');
    Route::middleware('scopes:assistance-management')->resource('/assistance_reports', 'AssistanceReportController');
    Route::resource('/service_reports', 'ServiceReportController');
    Route::resource('/user_permissions', 'UserPermissionController');
    Route::resource('/users','UserController');
});
    /*Route::get('/users','UserController@index');
    Route::get('users/{user}','UserController@show');
    Route::patch('users/{user}','UserController@update');
    Route::get('users/{user}/orders','UserController@showOrders');
    Route::patch('products/{product}/units/add','ProductController@updateUnits');
    Route::patch('orders/{order}/deliver','OrderController@deliverOrder');
    Route::resource('/orders', 'OrderController');
    Route::resource('/products', 'ProductController')->except(['index','show']);*/