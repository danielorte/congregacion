<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistanceReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistance_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->tinyInteger('meeting');
            $table->integer('week_1')->default(0);
            $table->integer('week_2')->default(0);
            $table->integer('week_3')->default(0);
            $table->integer('week_4')->default(0);
            $table->integer('week_5')->default(0);
            $table->integer('total')->default(0);
            $table->integer('average')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistance_reports');
    }
}
