<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('hours');
            $table->integer('minutes');
            $table->integer('preacher_id');
            $table->integer('publications')->default(0);
            $table->integer('videos')->default(0);
            $table->integer('visits')->default(0);
            $table->integer('studies')->default(0);
            $table->boolean('auxiliar_pionner')->default(false);
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_reports');
    }
}
