<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number', 5);
            $table->string('location', 120);
            $table->string('comments', 500)->nullable();;
            $table->integer('territory_id')->nullable();
            $table->boolean('status_1')->default(false);
            $table->boolean('status_2')->default(false);
            $table->boolean('status_3')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blocks');
    }
}
