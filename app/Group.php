<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'number','captain_1','captain_2',
    ];

    public function preachers()
    {
        return $this->hasMany('App\Preacher');
    }

    public function captain1()
    {
        return $this->belongsTo('App\Preacher','captain_1');
    }

    public function captain2()
    {
        return $this->belongsTo('App\Preacher','captain_2');
    }
}
