<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceReport extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'date','hours','minutes','preacher_id','publications','videos','visits','studies','auxiliar_pionner','user_id',
    ];

    public function preacher()
    {
        return $this->belongsTo('App\Preacher');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
