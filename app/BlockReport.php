<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockReport extends Model
{
    protected $fillable = [
        'user_id','block_id','status','value'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function block()
    {
        return $this->belongsTo('App\Block');
    }
}
