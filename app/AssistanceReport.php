<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistanceReport extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'date','meeting','week_1','week_2','week_3','week_4','week_5','total','average'
    ];
}
