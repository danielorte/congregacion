<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'number','location','comments','territory_id','status_1','status_2','status_3'
    ];

    public function territory()
    {
        return $this->belongsTo('App\Territory');
    }
}
