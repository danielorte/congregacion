<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Territory extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'number'
    ];

    public function blocks()
    {
        return $this->hasMany('App\Block');
    }
}
