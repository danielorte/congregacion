<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Preacher extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name','group_id','elder','ministerial','regular_pioneer','auxiliar_pioneer','status'
    ];

    public function group()
    {
        return $this->belongsTo('App\Group');
    }
}
