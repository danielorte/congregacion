<?php

namespace App\Http\Controllers;

use App\AssistanceReport;
use Illuminate\Http\Request;

class AssistanceReportController extends Controller
{
    public function index(Request $request)
    {
        $query = AssistanceReport::select('*');
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
        }
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $res = AssistanceReport::create($request->only([
            'date','meeting','week_1','week_2','week_3','week_4','week_5',
        ]));

        $res->total = array_sum([$res->week_1, $res->week_2, $res->week_3, $res->week_4, $res->week_5]);
        $res->average = ceil($res->total/5);
        $res->save();

        return response()->json([
            'status' => (bool) $res,
            'data'   => $res,
            'message' => $res ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(AssistanceReport $assistanceReport)
    {
        return response()->json($assistanceReport,200); 
    }

    public function update(Request $request, AssistanceReport $assistanceReport)
    {
        $status = $assistanceReport->update(
            $request->only(['date','meeting','week_1','week_2','week_3','week_4','week_5'])
        );
        
        $res = $assistanceReport;
        $res->total = array_sum([$res->week_1, $res->week_2, $res->week_3, $res->week_4, $res->week_5]);
        $res->average = ceil($res->total/5);
        $res->save();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(AssistanceReport $assistanceReport)
    {
        $status = $assistanceReport->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
