<?php

namespace App\Http\Controllers;

use App\BlockReport;
use Illuminate\Http\Request;

class BlockReportController extends Controller
{
    public function index(Request $request)
    {
        $query = BlockReport::with(['user','block']);
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
        }
        $query->orderBy('created_at','desc');
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $res = BlockReport::create($request->only(['user_id','block_id','status','value']));

        return response()->json([
            'status' => (bool) $res,
            'data'   => $res,
            'message' => $res ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(BlockReport $blockReport)
    {
        return response()->json(BlockReport::with(['user','block'])->find($blockReport->id),200); 
    }

    public function destroy(BlockReport $blockReport)
    {
        $status = $blockReport->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
