<?php

namespace App\Http\Controllers;

use Auth,DB;
use App\ServiceReport,App\Preacher;
use Illuminate\Http\Request;

class ServiceReportController extends Controller
{
    public function index(Request $request)
    {
        $query = ServiceReport::select('service_reports.*')->with(['user','preacher.group']);
        $ids = ServiceReport::select('service_reports.id');
        $query->join('preachers','preachers.id','=','service_reports.preacher_id');
        $query->leftJoin('groups','groups.id','=','preachers.group_id');
        $ids->join('preachers','preachers.id','=','service_reports.preacher_id');
        $ids->leftJoin('groups','groups.id','=','preachers.group_id');
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                            $ids->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else{
                            $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                            $ids->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                    }
                }
            }
        }
        $query->orderBy('date','desc')->orderBy('groups.number')->orderBy('preachers.name');
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            $totals = collect([
                'totals' => ServiceReport::selectRaw('
                    sum(publications) as publications,
                    sum(videos) as videos,
                    sum(visits) as visits,
                    sum(studies) as studies,
                    ifnull(sum(hours),0) as hours,
                    ifnull(sum(minutes),0) as minutes
                ')
                ->whereIn('id',$ids->get())->first(),
            ]);
            $data = $totals->merge($query->paginate($paginate));
            return response()->json($data,200);
        }
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $serviceReport = $request->only([
            'date','hours','minutes','preacher_id','publications','videos','visits','studies','auxiliar_pionner',            
        ]);
        $serviceReport['user_id'] = Auth::user()->id;
        $res = ServiceReport::create($serviceReport);

        return response()->json([
            'status' => (bool) $res,
            'data'   => $res,
            'message' => $res ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(ServiceReport $serviceReport)
    {
        return response()->json(ServiceReport::with(['user','preacher'])->find($serviceReport->id),200); 
    }

    public function update(Request $request, ServiceReport $serviceReport)
    {
        $status = $serviceReport->update(
            $request->only([
                'date','hours','minutes','preacher_id','publications','videos','visits','studies','auxiliar_pionner'
            ])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(ServiceReport $serviceReport)
    {
        $status = $serviceReport->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
    public function getChecksum() 
    {
        return DB::select(DB::raw('CHECKSUM TABLE service_reports'))[0]->Checksum;
    }

    public function pendingReports(){
        $date = date('Y-m', strtotime("first day of previous month"));
        $ids=ServiceReport::select('preacher_id')->whereRaw('date LIKE "'.$date.'-%"');
        $query=Preacher::whereNotIn('id',$ids)->where('status','!=',3)->get();
        return $query;
    }
	public function notify()
	{
        session_write_close(); 
        ignore_user_abort(TRUE);
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache');
		function sendMsg($id, $datos) {
			echo "id: $id" . PHP_EOL;
			echo "data: $datos\n";
			echo PHP_EOL;
            ob_flush();
			flush();
		}
		$startedAt = time();
		$checksum=-1;
		do {
            if ((time() - $startedAt) > 60) { die(); }
            else if (connection_aborted()) { die(); }
            else if (connection_status()!=0) { die(); }
            else {
                $checksum_actual=$this->getChecksum();
                if($checksum != $checksum_actual){
                    $datos=["id"=>$startedAt,"service_reports"=>$this->pendingReports(),"status"=>connection_status()];
                    $checksum=$checksum_actual;
                    sendMsg($startedAt,json_encode($datos));
                }
                else{
                    sendMsg($startedAt,json_encode(["id"=>$startedAt]));
                }
            }
            sleep(2);
		} while(true);
	}
}
