<?php

namespace App\Http\Controllers;

use App\Preacher;
use Illuminate\Http\Request;

class PreacherController extends Controller
{
    public function index(Request $request)
    {
        $query = Preacher::with(['group']);
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
        }
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        $query->orderBy('name');
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $res = Preacher::create($request->only([
            'name','group_id','elder','ministerial','regular_pioneer','auxiliar_pioneer','status'
        ]));

        return response()->json([
            'status' => (bool) $res,
            'data'   => $res,
            'message' => $res ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(Preacher $preacher)
    {
        return response()->json(Preacher::with(['group'])->find($preacher->id),200); 
    }

    public function update(Request $request, Preacher $preacher)
    {
        $status = $preacher->update(
            $request->only([
                'name','group_id','elder','ministerial','regular_pioneer','auxiliar_pioneer','status'
            ])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(Preacher $preacher)
    {
        $status = $preacher->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
