<?php

namespace App\Http\Controllers;

use Auth, DB;
use App\Block;
use App\BlockReport;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    public function index(Request $request)
    {
        $query = Block::with(['territory']);
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
        }
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $res = Block::create($request->only(['number','location','comments','territory_id']));

        return response()->json([
            'status' => (bool) $res,
            'data'   => $res,
            'message' => $res ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(Block $block)
    {
        return response()->json(Block::with(['territory'])->find($block->id),200); 
    }

    public function update(Request $request, Block $block)
    {
        $log = [];
        if($request->status_1 != $block->status_1){
            $log[] = [ 
                'user_id' => Auth::user()->id,
                'status' => 1,
                'value' => $request->status_1,
                'block_id' => $block->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        if($request->status_2 != $block->status_2){
            $log[] = [ 
                'user_id' => Auth::user()->id,
                'status' => 2,
                'value' => $request->status_2,
                'block_id' => $block->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        if($request->status_3 != $block->status_3){
            $log[] = [ 
                'user_id' => Auth::user()->id,
                'status' => 3,
                'value' => $request->status_3,
                'block_id' => $block->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        $status = $block->update(
            $request->only(['number', 'location','comments','territory_id','status_1','status_2','status_3'])
        );
        if(count($log) > 0){
            BlockReport::insert($log);
        }
        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(Block $block)
    {
        $status = $block->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }

    public function getChecksum() 
    {
        return DB::select(DB::raw('CHECKSUM TABLE block_reports'))[0]->Checksum;
    }

	public function notify()
	{
        session_write_close(); 
        ignore_user_abort(TRUE);
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache');
		function sendMsg($id, $datos) {
			echo "id: $id" . PHP_EOL;
			echo "data: $datos\n";
			echo PHP_EOL;
			ob_flush();
			flush();
		}
		$startedAt = time();
		$checksum=-1;
		do {
            if ((time() - $startedAt) > 60) { die(); }
            else if (connection_aborted()) { die(); }
            else if (connection_status()!=0) { die(); }
            else {
                $checksum_actual=$this->getChecksum();
                if($checksum != $checksum_actual){
                    $datos=["id"=>$startedAt,"blocks"=>Block::all(),"status"=>connection_status()];
                    $checksum=$checksum_actual;
                    sendMsg($startedAt,json_encode($datos));
                }
                else{
                    sendMsg($startedAt,json_encode(["id"=>$startedAt]));
                }
            }
            sleep(2);
		} while(true);
	}
}
