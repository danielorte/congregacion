/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
el: '#app'
});*/
import Vue from 'vue'
import VueRouter from 'vue-router'

//import datePicker from 'vue-bootstrap-datetimepicker';
//import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
//Vue.use(datePicker);

Vue.use(VueRouter)

import Util from './utils';
window.Util = Util;
import App from './views/App'
import cfenv from './config/cfenv'

const router = new VueRouter({
    mode: 'history',
    routes: cfenv.getRoutes(),
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('mapa.jwt') == null) {
            if(to.fullPath == '/'){
                next({
                    path: '/mapa',
                    params: { nextUrl: to.fullPath }
                })
            }else {
                next({
                    path: '/login',
                    params: { nextUrl: to.fullPath }
                })
            }
        } else {
            let user = JSON.parse(localStorage.getItem('mapa.user'))
            if (to.matched.some(record => record.meta.requiredScope)) {
                if(user.user_permissions.find(item => item.permission.permission == to.meta.requiredScope)){
                    next();
                }
            }
            else next()
        }
    } else {
        next()
    }
})

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});